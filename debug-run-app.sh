#!/bin/bash

## Применяйте в случае дебага

#чистим
mvn clean
#собираем вместе с jar'ником
mvn install

#удаляем все образы и контейнеры
# shellcheck disable=SC2046
docker stop $(docker ps -aq)
# shellcheck disable=SC2046
docker rm $(docker ps -aq)
# shellcheck disable=SC2046
docker rmi $(docker images -aq)

#поднимаем компоуз, а вместе с ним и докер файл со слоями окружения
docker-compose -f docker-compose.yml up -d
