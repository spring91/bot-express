# Тестирование

Как можно знать, тестирование приложений происходит по двум трекам:

- Модульное тестирование(изолированное тестирование отдельных блоков кода);
- Интеграционное. Это тестирование взаимодействия нескольких классов, выполняющих вместе какую-то работу(одновременное
  тестирование нескольких слоев в полиномиальной зависимости).

Тестирование крупных REST приложений чаще всего происходит по интеграционному треку(зависит от "Тестовой пирамиды" или
[TDD](tdd.md) модели). Интеграционное тестирование так же бывает нескольких видов:

- Полное
- Фокусное

### Полное тестирование Spring

Полное тестирование представляет собой одновременный тест всех слоев приложения.
Утвердим основные моменты кода для интеграционного тестирования:

- Отключение Security сервиса(если доступ к методам закрыт при не аутентифицированных запросах, то будьте уверены -
  защита работает нужным образом = тестить одни аннотации не нужно). Аннотация @WithMockUser(authorities={}) позволяет
  зарядить в контекст мнимого пользователя с любыми правами доступа. @WithUserDetails(value = "admin") так же позволяет
  предоставить пользователя с правами, но только уже существующего(аннотация берет юзера из бд по атрибуту(имени, id и
  т.д))
- Отключение тяжеловесных компонентов в ходе тестов(сервер). С помощью фреймворка Mockito подменяем сервер приложения и
  тестируем только уровень ниже. Таким образом, используется почти весь стек, и код будет вызываться точно так же, как
  если бы он обрабатывал реальный HTTP-запрос, но без затрат на запуск сервера. Для этого используется Spring MockMvc,
  такая заглушка настраивается с помощью аннотации *@AutoConfigureMockMvc*.
- Поднятие целостного окружения в целях приближения тестов к реалиям. Аннотация *@SpringBootTest* - позволяет поднимать
  весь контекст приложения для тестов.

### Фокусное тестирование Spring

Фокусный подход предполагает тестирование отдельных слоев приложения в изоляции от других слоев(только доступ к данным,
только веб и т.д.)

- Тестирование только сервисного слоя происходит при изоляции его от всех остальных слоев. Это достигается при помощи
  аннотации *@MockBean*, которая подменяет бины в контексте приложения(к примеру, мы можем отключить слой общения с
  базой данных).

      @MockBean
      private PersonRepository repository;

- Для тестирования только REST слоя можно использовать аннотацию @WebMvcTest. Эта аннотация создаст только бин
  контроллера, а репозиторий и остальные бины создавать не будет. Остальные внедряемые бины можно заменить все той же
  аннотацией *@MockBean*.

Таким образом, можно тестировать только конкретную узкую логику приложения, прописыва

## Тестирование с помощью контейнеров

Разработчики задаются вопросом о том, как более эффективно проводить интеграционное тестирование, чтобы тестовая база
данных лишний раз не кушала ресурсы сервера(и не маячила после тестов в общей системе). Ответ нашелся спустя время -
тестовый контейнер(при участии Docker'а конечно же).

TestContainers — это Java-библиотека, которая поддерживает тесты JUnit и предоставляет легкие, временные экземпляры
основных баз данных, веб-браузеров для различных тестов или чего угодно еще, что можно запускать в Docker-контейнере.

В результате мы можем писать автономные интеграционные тесты, которые зависят от внешних ресурсов. К примеру докер
создает нам контейнер на сервере, заполняет его, проводит тесты, затем удаляет данные, удаляет контейнер.

