# Архитектура REST приложения

## Архитектура клиент-сервер

В общем случае архитектура определяет лишь общие принципы взаимодействия между компьютерами, детали взаимодействия
определяют различные протоколы. Данная концепция нам говорит, что нужно разделять машины в сети на клиентские, которым
всегда что-то надо, и на серверные, дающие то, что надо. При этом взаимодействие всегда начинает клиент, а правила, по
которым происходит взаимодействие, описывает протокол.

Наше приложение реализовано как техническая back-end(серверная) часть, которая будет связываться с front-end по
средствам REST взаимодействия. Рассмотрим более подробно серверную архитектуру.

## Шестиугольная архитектура(Портов и Адаптеров)

Термин Hexagonal Architecture (гексагональная, шестиугольная архитектура) придумал в 2006 году Алистер Коберн (Alistair
Cockburn). Этот архитектурный стиль также известен как Ports And Adapters Architecture (архитектура портов и адаптеров).
Говоря простыми словами, компоненты приложения взаимодействуют через множество конечных точек (портов). Для обработки
запросов у должны быть адаптеры, соответствующие портам. Т.е. в приложении присутствует несколько слоев, каждый из
которых взаимодействует друг с другом через реализации портов - адаптеры.

Эту архитектуру можно схематически представить в виде шестиугольника с бизнес-логикой в самом центре (в ядре),
окруженную объектами с которыми она взаимодействует, и компонентами, управляющими ею, предоставляя входные данные.

![img.png](layers.png)

В реальной жизни с вашим приложением взаимодействуют и предоставляют входные данные пользователи, вызовы API,
автоматизированные скрипты и модульное тестирование. Если ваша бизнес-логика смешана с логикой пользовательского
интерфейса, то вы столкнетесь с многочисленными проблемами. Например, будет сложно переключить ввод данных с
пользовательского интерфейса на модульные тесты.

Гексагональная архитектура выделяет в приложении три слоя:

- Домен (domain). Слой содержит основную бизнес-логику. Он не должен знать детали реализации внешних слоев;
- Приложение (application). Слой действует как связующее звено между слоями домена и инфраструктуры;
- Инфраструктура (framework). Реализация взаимодействия домена с внешним миром. Внутренние слои выглядят для него как
  черный ящик.

Согласно этой архитектуре, с приложением взаимодействуют два типа участников: основные (driver) и вторичные (driven).
Основные действующие лица отправляют запросы и управляют приложением (например, пользователи или автоматизированные
тесты). Вторичные обеспечивают инфраструктуру для связи с внешним миром (это адаптеры базы данных, TCP- или
HTTP-клиенты). Это можно представить так:

![img.png](participants.png)

### Подробнее о слоях

- *Entity* Доменный слой. Именно здесь лежат POJO-классы, описывающие объекты-сущности системы.(POJO - старый добрый
  джава объект).
- *Repository* Слой доступа к данным. В нём будем размещать Data Access Objects – объекты доступа к данным. Вернее
  методы для доступа к БД уже автоматически даны нам из коробки, поэтому слой представлен в виде интерфейса(Repository).
- *REST* Веб-слой приложения(Прикладной уровень). Здесь лежат классы-контроллеры, описывающие порядок взаимодействия
  пользователя с системой через веб, DTO для общения через веб и тд.
- *Config* Инфраструктурный уровень. Уровень инфраструктуры содержит логику, необходимую для запуска приложения.
- *Service* Сервис-слой приложения, который объединяет бизнес-функции и решает общие проблемы, такие как безопасность
  или транзакции. Содержит интерфейсы, в которых описан функционал приложения(Управляющий порт). Также содержит одну или
  несколько практических реализаций этих интерфейсов(Управляемые порты). В данных классах мы прописываем логику
  приложения и его функционирования.

### Примерный проход данных из одного слоя в другой

Архитектура допускает зависимости от внешних уровней к внутренним, но не наоборот. Для конечной точки REST поток
запроса может выглядеть следующим образом:

1) Запрос отправляется контроллеру на уровне «инфраструктуры».
2) Контроллер десериализует запрос и - в случае успеха - запрашивает результат у соответствующего сервиса на уровне
   сервисы.
3) Служба проверяет, есть ли у текущего пользователя разрешение на вызов функции, и инициализирует транзакцию базы
   данных (при необходимости).
4) Затем он извлекает данные из репозиториев домена, манипулирует ими и, возможно, сохраняет их обратно в репозиторий.
5) Сервис также может вызывать несколько репозиториев, преобразовывать и агрегировать результаты.
6) Репозиторий на уровне домена возвращает бизнес-объекты. Этот уровень отвечает за поддержание всех объектов в
   допустимом состоянии.
7) В зависимости от ответа сервиса, который является допустимым результатом или исключением, уровень инфраструктуры
   сериализует ответ.