### Базовые технологии проекта

В файле представлена информация об общей структуре программы и логике в частности.

Порядок чтения статей:

Основа и архитектура:

- [architecture](rest/architecture.md)
- [springboot](sping-boot/spring-boot.md)

Транзакции/AOP:

- [AOP](db/aop/AOP.md)
- [transactional](db/aop/AOP.md)

Базы данных:

Вообще, теорию БД стоит почитать отдельно, тк данный материал исключительно общего представления о технологиях Spring'а

- [JPA](db/jpa/JPA.md)
- [YML](db/YML.md)
- [repository](db/repository.md)
- [migration](db/migrationDB.md)

REST :

- [REST](rest/restinfo/REST.md)
- [swagger](rest/restinfo/open-api/swagger.md)
- [dto](rest/restinfo/DTO.md)

Security:

- [security1](security/security-base.md)
- [security2](security/security-in-action.md)

Исключения и логи:

- [exception&validation](validation/validation.md)
- [handling](validation/handling.md)

Тестирование:

- [testing](testing/testing.md)