package ru.ershov.botexpress.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.annotation.PostConstruct;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Класс создан для упрощения вызова методов тестирования с помощью MockMvcRequest
 * Отдельные методы просто вынесены в данный класс, чтобы не засорять код тестирования
 */
@Component
@RequiredArgsConstructor
public class MockMvcHelper {

    private final ObjectMapper injectedObjMapper;
    private static ObjectMapper objectMapper;

    @PostConstruct
    private void initStaticObjectMapper() {
        objectMapper = this.injectedObjMapper;
    }

    /**
     * Метод позволяет преобразовать объект в Json и использовать его в подмененном post запросе в качестве body
     *
     * @param body        тело объекта
     * @param urlTemplate путь запроса
     * @param uriVars     дополнительные переменные запроса
     * @return {@link MockHttpServletRequestBuilder}
     */
    public static MockHttpServletRequestBuilder postJson(Object body,
                                                         String urlTemplate,
                                                         Object... uriVars) {
        try {
            return post(urlTemplate, uriVars).contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(body));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}