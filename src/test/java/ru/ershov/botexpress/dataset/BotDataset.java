package ru.ershov.botexpress.dataset;

/**
 * Интерфейс заполнения данных сущности бота
 */
public interface BotDataset {

    void createData();

    void removeData();
}