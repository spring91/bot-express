package ru.ershov.botexpress.dataset.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.dataset.BotDataset;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.repository.BotModelRepository;
import ru.ershov.botexpress.store.repository.BotRepository;
import ru.ershov.botexpress.store.repository.ParkRepository;

import java.math.BigDecimal;
import java.util.List;

import static ru.ershov.botexpress.store.entity.enums.BotStatus.AVAILABLE;

@Component
@RequiredArgsConstructor
public class BotDatasetImpl implements BotDataset {

    private final BotRepository botRepository;
    private final ParkRepository parkRepository;
    private final BotModelRepository modelRepository;

    @Override
    @Transactional
    public void createData() {
        botRepository.saveAll(getPresetBots());
    }

    @Override
    @Transactional
    public void removeData() {
        botRepository.deleteAllInBatch();
    }

    private List<Bot> getPresetBots() {
        return List.of(
                Bot.builder()
                        .status(AVAILABLE)
                        .maintenanceCost(BigDecimal.valueOf(0))
                        .model(modelRepository.getReferenceById(1L))
                        .park(parkRepository.getReferenceById(1L))
                        .build(),
                Bot.builder()
                        .status(AVAILABLE)
                        .maintenanceCost(BigDecimal.valueOf(0))
                        .model(modelRepository.getReferenceById(2L))
                        .park(parkRepository.getReferenceById(2L))
                        .build(),
                Bot.builder()
                        .status(AVAILABLE)
                        .maintenanceCost(BigDecimal.valueOf(0))
                        .model(modelRepository.getReferenceById(3L))
                        .park(parkRepository.getReferenceById(3L))
                        .build(),
                Bot.builder()
                        .status(AVAILABLE)
                        .maintenanceCost(BigDecimal.valueOf(0))
                        .model(modelRepository.getReferenceById(4L))
                        .park(parkRepository.getReferenceById(4L))
                        .build(),
                Bot.builder()
                        .status(AVAILABLE)
                        .maintenanceCost(BigDecimal.valueOf(0))
                        .model(modelRepository.getReferenceById(5L))
                        .park(parkRepository.getReferenceById(5L))
                        .build()
        );
    }
}