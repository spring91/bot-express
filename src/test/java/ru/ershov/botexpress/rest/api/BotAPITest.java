package ru.ershov.botexpress.rest.api;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.ershov.botexpress.common.DatabaseContainer;
import ru.ershov.botexpress.dataset.BotDataset;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.entity.BotModel;
import ru.ershov.botexpress.store.entity.Park;
import ru.ershov.botexpress.store.repository.BotModelRepository;
import ru.ershov.botexpress.store.repository.BotRepository;
import ru.ershov.botexpress.store.repository.ParkRepository;

import javax.persistence.EntityNotFoundException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.ershov.botexpress.utils.MockMvcHelper.postJson;

@Slf4j
@Testcontainers
@ActiveProfiles("test")
@WithUserDetails(value = "admin")
@AutoConfigureMockMvc(addFilters = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BotAPITest {

    private static final Integer API_VERSION = 1;
    private static final String COMMON_API_URI = "/bots";

    @Container
    private static final PostgreSQLContainer<DatabaseContainer> container = DatabaseContainer.getInstance();

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BotDataset botDataset;
    @Autowired
    private BotModelRepository modelRepository;
    @Autowired
    private ParkRepository parkRepository;
    @Autowired
    private BotRepository botRepository;

    @BeforeEach
    public void setUp() {
        botDataset.createData();
    }

    @AfterEach
    public void tearDown() {
        botDataset.removeData();
    }

    /**
     * Метод получения сущности модели бота
     */
    @Transactional(readOnly = true)
    BotModel getAnyModel() {
        return modelRepository.findAll().stream()
                .findAny()
                .orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Метод получения сущности парка
     */
    @Transactional(readOnly = true)
    Park getAnyPark() {
        return parkRepository.findAll().stream()
                .findAny()
                .orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Метод получения сущности бота
     */
    @Transactional(readOnly = true)
    Bot getAnyBot() {
        return botRepository.findAll().stream()
                .findAny()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Test
    @DisplayName("Создание бота")
    void create() throws Exception {
        final var modelId = getAnyModel().getId();
        final var parkId = getAnyPark().getId();

        final var botRqDto = BotRequestDTO.builder()
                .modelId(modelId)
                .parkId(parkId)
                .build();

        //проверяем, что возвращаемые методом данные равны тем, которые мы отправили
        mockMvc.perform(postJson(botRqDto, COMMON_API_URI))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.modelId").value(modelId))
                .andExpect(jsonPath("$.parkId").value(parkId))
                .andReturn();
    }

    @Test
    @DisplayName("Создание бота, пустой запрос")
    void createNullRequest() throws Exception {
        mockMvc.perform(postJson(null, COMMON_API_URI))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Создание бота пустой идентификатор парка")
    void createBuilderNullField() throws Exception {
        final var botRqDto = BotRequestDTO.builder()
                .modelId(getAnyModel().getId())
                .build();

        mockMvc.perform(postJson(botRqDto, COMMON_API_URI))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Создание бота с неизвестным парком")
    void createUnknownPark() throws Exception {
        final var botRqDto = BotRequestDTO.builder()
                .modelId(getAnyModel().getId())
                .parkId(2544264875L)
                .build();

        mockMvc.perform(postJson(botRqDto, COMMON_API_URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Создание бота с нулевыми полем")
    void createNullField() throws Exception {
        final var botRqDto = BotRequestDTO.builder().build();
        mockMvc.perform(postJson(botRqDto, COMMON_API_URI))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    //TODO: сделать проверку авторизации внутренним классом
    @Test
    @DisplayName("Получение бота по id")
    void getById() throws Exception {
        final var bot = getAnyBot();
        final var botId = bot.getId();
        final var botStatus = bot.getStatus();
        final var botMaintenanceCost = bot.getMaintenanceCost();
        final var botModelId = bot.getModel().getId();
        final var botParkId = bot.getPark().getId();
        //mockMvc.perform()
          //              .andExpect(status().isForbidden());

        mockMvc.perform(get(COMMON_API_URI + "/{id}", botId))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(botId.intValue()))
                .andExpect(jsonPath("$.status").value(botStatus.name()))
                .andExpect(jsonPath("$.maintenanceCost").value(botMaintenanceCost.doubleValue()))
                .andExpect(jsonPath("$.modelId").value(botModelId.intValue()))
                .andExpect(jsonPath("$.parkId").value(botParkId.intValue()))
                .andReturn();
    }

    @Test
    @DisplayName("Получение несуществующего бота по id")
    void getByIncorrectId() throws Exception {
        mockMvc.perform(get(COMMON_API_URI + "/" + 101235126L, API_VERSION)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Получение бота по id = null")
    void getByNullId() throws Exception {
        mockMvc.perform(get(COMMON_API_URI + "/" + null, API_VERSION)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Получение всех ботов")
    void getAll() throws Exception {
        mockMvc.perform(get(COMMON_API_URI + "/from-park/" + getAnyPark().getId(), API_VERSION)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }
}