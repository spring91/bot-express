--добавление расширяющей функции для генерации uuid
create extension if not exists pgcrypto;
create extension if not exists "uuid-ossp";
drop table if exists "user" cascade;
create table "user"
(
    id         uuid default gen_random_uuid() not null,
    "password" varchar(255)                   not null,
    username   varchar(255)                   not null,
    primary key (id)
);
--insert users
INSERT INTO "user"(id, "password", username)/*pas = 12345*/
VALUES ('33d7a519-8919-4d0a-8c56-f4c0b2af98e2', '$2a$04$KWM/bvuucdBcfy1qGCAg8e7WUaW5fLINdBLKKLtzQvZ95BYdR3wLi',
        'admin'),
       ('4d635dfa-b0da-42a0-b55a-5dd2114fb980', '$2a$04$KWM/bvuucdBcfy1qGCAg8e7WUaW5fLINdBLKKLtzQvZ95BYdR3wLi',
        'user');
