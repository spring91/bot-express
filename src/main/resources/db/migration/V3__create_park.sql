drop table if exists park cascade;
create table park
(
    id                      bigint generated by default as identity                    not null,
    "name"                  varchar(40)                                                not null,
    latitude                double precision check (latitude >= 0 and latitude <= 90)  not null,
    longitude               double precision check (latitude >= 0 and latitude <= 180) not null,
    profits                 numeric(19, 2) check (profits >= 0)                        not null,
    total_maintenance_costs numeric(19, 2) check (total_maintenance_costs >= 0)        not null,
    primary key (id),
    unique ("name")
);
--insert parks
INSERT into park(id, "name", latitude, longitude, profits, total_maintenance_costs)
VALUES (1, 'Central', 53.22404062425825, 44.89527750914538, 0, 0),
       (2, 'Eastern', 53.2235203402152, 44.887470170588045, 0, 0),
       (3, 'Western', 53.224175511682084, 44.895521518169524, 0, 0),
       (4, 'South', 53.228465994701715, 44.92142750569861, 0, 0),
       (5, 'Northern', 53.21939638383713, 44.94941105434736, 0, 0);
