drop table if exists "role" cascade;
create table "role"
(
    id     bigint generated by default as identity      not null,
    "name" varchar(30) check ("name" ~ '^ROLE_[A-Z]+$') not null,
    primary key (id),
    unique ("name")
);
--insert roles
INSERT INTO "role"(id, "name")
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');

alter table if exists user_roles
    drop constraint if exists FK_to_role;
alter table if exists user_roles
    drop constraint if exists FK_to_user;
drop table if exists user_roles cascade;
create table user_roles
(
    user_id uuid   not null,
    role_id bigint not null,
    primary key (user_id, role_id),
    constraint FK_to_role foreign key (role_id) references "role",
    constraint FK_to_user foreign key (user_id) references "user"
);
--insert users roles
INSERT INTO user_roles(user_id, role_id)
VALUES ('33d7a519-8919-4d0a-8c56-f4c0b2af98e2', 1),
       ('4d635dfa-b0da-42a0-b55a-5dd2114fb980', 2);
