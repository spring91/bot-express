package ru.ershov.botexpress.utils.validation;

/**
 * Метка для указания нужных аннотаций валидации.
 * Отключает неиспользуемые группы аннотаций.
 */
public interface OnUpdate {
}
