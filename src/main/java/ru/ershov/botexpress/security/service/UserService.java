package ru.ershov.botexpress.security.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.ershov.botexpress.security.rest.dto.request.UserRequestDTO;
import ru.ershov.botexpress.security.rest.dto.response.UserResponseDTO;

import java.util.UUID;

/**
 * Сервис пользователя
 */
public interface UserService extends UserDetailsService {

    /**
     * Создание записи о пользователе
     *
     * @param userRqDto сущность пользователя с данными для сохранения
     * @return сохраненная сущность пользователя
     */
    UserResponseDTO create(UserRequestDTO userRqDto);

    /**
     * Получение записи о пользователе по идентификатору
     *
     * @param id идентификатор пользователя
     * @return сущность пользователя
     */
    UserResponseDTO getById(UUID id);

    /**
     * Получение всех записей о пользователях
     *
     * @param pageable параметры страницы
     * @return страница с пользователями
     */
    Page<UserResponseDTO> getAll(Pageable pageable);

    /**
     * Изменение записи о сущности пользователя.
     * Пользователь может менять только свои личные данные: пароль и никнейм
     *
     * @param id        идентификатор сущности пользователя
     * @param userRqDTO Dto пользователя с данными для изменения
     * @return измененная Dto пользователя
     */
    UserResponseDTO update(UUID id, UserRequestDTO userRqDTO);
}
