package ru.ershov.botexpress.security.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.security.rest.dto.request.UserRequestDTO;
import ru.ershov.botexpress.security.rest.dto.response.UserResponseDTO;
import ru.ershov.botexpress.security.rest.mapper.UserMapper;
import ru.ershov.botexpress.security.service.UserService;
import ru.ershov.botexpress.security.store.entity.UserEntity_;
import ru.ershov.botexpress.security.store.entity.securityprovider.SecurityUser;
import ru.ershov.botexpress.security.store.repository.RoleRepository;
import ru.ershov.botexpress.security.store.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.ershov.botexpress.security.store.entity.enums.RoleName.ROLE_USER;
import static ru.ershov.botexpress.utils.BeanUtilsHelper.getIgnoredPropertyNames;


@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    public static final String USER_IS_NOT_FOUND = "Пользователь с id = %s не найден";
    private static final String USER_IS_NOT_FOUND_MSG = "Пользователь с именем = %s не найден";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    @Override
    @Transactional
    public UserResponseDTO create(UserRequestDTO userRqDTO) {
        var user = userMapper.toEntity(userRqDTO);
        var role = roleRepository.getByName(ROLE_USER);
        user.setRoles(Set.of(role));
        return userMapper.toDTO(userRepository.save(user));
    }

    @Override
    public UserResponseDTO getById(UUID id) {
        return userRepository.findById(id)
                .map(userMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_IS_NOT_FOUND, id)));
    }

    @Override
    public Page<UserResponseDTO> getAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(userMapper::toDTO);
    }

    @Override
    @Transactional
    public UserResponseDTO update(UUID id, UserRequestDTO userRqDTO) {
        return userRepository.findById(id)
                .map(persistentUser -> {
                    var user = userMapper.toEntity(userRqDTO);
                    BeanUtils.copyProperties(user, persistentUser,
                            getIgnoredPropertyNames(
                                    user,
                                    UserEntity_.ID, UserEntity_.ROLES, UserEntity_.ORDERS
                            )
                    );
                    return persistentUser;
                })
                .map(userMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_IS_NOT_FOUND, id)));
    }

    /**
     * Метод получения пользователя из базы при аутентификации
     *
     * @param username имя пользователя
     * @return сущность пользователя, связанная с Security контекстом
     */
    @Override
    public SecurityUser loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_IS_NOT_FOUND_MSG, username)));
        var authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toSet());
        return SecurityUser.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(authorities)
                .build();
    }
}
