package ru.ershov.botexpress.security.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ershov.botexpress.utils.validation.OnUpdate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Входное dto юзера")
public class UserRequestDTO {

    @NotBlank(message = "Не указано имя пользователя",
            groups = {OnUpdate.class, Default.class})
    @Schema(description = "Никнейм пользователя")
    private String username;

    @NotBlank(message = "Не задан пароль пользователя")
    @Pattern(regexp = "^(?=\\w*[a-z])(?=\\w*[A-Z])(?=\\w*\\d)(?!\\w*\\W).{8,}$",
            message = "Пароль не подходит шаблону: латинские буквы и цифры в кол-ве 8 знаков",
            groups = {OnUpdate.class, Default.class})
    @Schema(description = "Пароль пользователя")
    private String password;
}