package ru.ershov.botexpress.security.rest.mapper;

import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.ershov.botexpress.security.rest.dto.request.UserRequestDTO;
import ru.ershov.botexpress.security.rest.dto.response.UserResponseDTO;
import ru.ershov.botexpress.security.store.entity.UserEntity;
import ru.ershov.botexpress.security.store.entity.RoleEntity;

import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {

    @Autowired
    protected PasswordEncoder passwordEncoder;

    public abstract UserResponseDTO toDTO(UserEntity user);

    @AfterMapping
    protected void mapRole(UserEntity user, @MappingTarget UserResponseDTO userRsDTO) {
        Optional.ofNullable(user.getRoles())
                .map(roles -> roles.stream()
                        .map(RoleEntity::getName)
                        .map(Enum::name)
                        .collect(Collectors.toSet())
                )
                .ifPresent(userRsDTO::setRoleNames);
    }

    public abstract UserEntity toEntity(UserRequestDTO userDTO);

    @AfterMapping
    protected void mapPassword(UserRequestDTO userRqDTO, @MappingTarget UserEntity user) {
        Optional.ofNullable(userRqDTO.getPassword())
                .ifPresent(password ->
                        user.setPassword(passwordEncoder.encode(password))
                );
    }
}
