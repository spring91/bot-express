package ru.ershov.botexpress.security.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Выходное DTO юзера")
public class UserResponseDTO {

    private UUID id;

    private String username;

    private Set<String> roleNames;
}
