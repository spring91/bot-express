package ru.ershov.botexpress.security.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.ershov.botexpress.security.rest.api.UserAPI;
import ru.ershov.botexpress.security.rest.dto.request.UserRequestDTO;
import ru.ershov.botexpress.security.rest.dto.response.UserResponseDTO;
import ru.ershov.botexpress.security.service.UserService;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController implements UserAPI {

    private final UserService userService;

    @Override
    public ResponseEntity<UserResponseDTO> create(UserRequestDTO userRqDTO) {
        log.info("POST:/auth : userRqDTO={}", userRqDTO);

        var userRsDto = userService.create(userRqDTO);
        var url = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(userRsDto.getId())
                .toUri();
        return ResponseEntity.created(url).body(userRsDto);
    }

    @Override
    public UserResponseDTO getById(UUID id) {
        log.info("GET:/auth/{id} : id={}", id);
        return userService.getById(id);
    }

    @Override
    public Page<UserResponseDTO> getAll(Pageable pageable) {
        log.info("GET:/auth : pageable={}", pageable);
        return userService.getAll(pageable);
    }

    @Override
    public UserResponseDTO update(UUID id, UserRequestDTO userRqDTO) {
        log.info("PATCH:/auth/{id} : id={}, userRqDTO={}", id, userRqDTO);
        return userService.update(id, userRqDTO);
    }
}
