package ru.ershov.botexpress.security.rest.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.ershov.botexpress.security.rest.dto.request.UserRequestDTO;
import ru.ershov.botexpress.security.rest.dto.response.UserResponseDTO;
import ru.ershov.botexpress.utils.validation.OnUpdate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Validated
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для управления пользователями")
@PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
public interface UserAPI {

    @Operation(summary = "Метод создания пользователя", method = "POST",
            responses = {
                    @ApiResponse(description = "CREATED", responseCode = "201",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = UserResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403")
            }
    )
    @PostMapping("/registration")
    @PreAuthorize("true")
    ResponseEntity<UserResponseDTO> create(@RequestBody @Valid UserRequestDTO userRqDTO);

    @Operation(summary = "Метод получения пользователя", method = "GET",
            security = @SecurityRequirement(name = "basic-auth"),
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = UserResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping("/{id}")
    @PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)" +
            "or #id == authentication.getPrincipal().getId()")
    UserResponseDTO getById(@PathVariable @NotNull UUID id);

    @Operation(summary = "Получение всех пользователей", method = "GET",
            security = @SecurityRequirement(name = "basic-auth"),
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping
    @PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
    Page<UserResponseDTO> getAll(@ParameterObject Pageable pageable);

    @Operation(summary = "Метод обновления пользователя", method = "PATCH",
            security = @SecurityRequirement(name = "basic-auth"),
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = UserResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @PatchMapping("/{id}")
    @PreAuthorize("#id == authentication.getPrincipal().getId()")
    UserResponseDTO update(@PathVariable @NotNull UUID id,
                           @RequestBody @Validated(OnUpdate.class) UserRequestDTO userRqDTO);
}
