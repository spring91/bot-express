package ru.ershov.botexpress.security.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ershov.botexpress.security.store.entity.UserEntity;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> findByUsername(String username);
}
