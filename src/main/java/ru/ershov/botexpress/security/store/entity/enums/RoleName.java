package ru.ershov.botexpress.security.store.entity.enums;

/**
 * Имя роли
 */
public enum RoleName {

    ROLE_ADMIN,
    ROLE_USER
}
