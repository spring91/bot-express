package ru.ershov.botexpress.security.store.entity;

import lombok.*;
import ru.ershov.botexpress.security.store.entity.enums.RoleName;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"role\"")
public class RoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Имя роли
     */
    @Enumerated(EnumType.STRING)
    private RoleName name;

    /**
     * Пользователи роли
     */
    @ManyToMany(mappedBy = "roles")
    private Set<UserEntity> users;
}
