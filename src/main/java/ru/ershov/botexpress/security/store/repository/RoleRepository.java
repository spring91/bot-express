package ru.ershov.botexpress.security.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ershov.botexpress.security.store.entity.RoleEntity;
import ru.ershov.botexpress.security.store.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity getByName(RoleName name);
}
