package ru.ershov.botexpress.security.store.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import ru.ershov.botexpress.store.entity.Order;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"user\"")
public class UserEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    /**
     * Никнейм пользователя
     */
    private String username;

    /**
     * Пароль
     */
    private String password;

    /**
     * Роли пользователя
     */
    @ManyToMany
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id")
    )
    private Set<RoleEntity> roles;

    /**
     * Заказы пользователя
     */
    @OneToMany(mappedBy = "user")
    private List<Order> orders;
}
