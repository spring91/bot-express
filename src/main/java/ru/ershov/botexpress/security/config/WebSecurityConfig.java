package ru.ershov.botexpress.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import ru.ershov.botexpress.security.service.UserService;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    private static final String[] WHITE_ALBUM = {
            // main list
            "/",
            "/users/registration",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**"
    };

    /**
     * Конфигурация фильтров выглядит следующим образом:
     * <ol>
     *     <li>Отключение csrf защиты.</li>
     *     <li>Предоставление доступа всем пользователям к ссылкам из WHITE_ALBUM(без наличия каких-либо прав).</li>
     *     <li>Для пользования остальными ссылками нужно быть аутентифицированным пользователем.</li>
     *     <li>Включение httpBasic.</li>
     *     <li>При logout удалять куки и сессию.</li>
     *     <li>При ошибке аутентификации или авторизации кидать нужный статус.</li>
     * </ol>
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .csrf().disable()
                .authorizeRequests()
                    .antMatchers(WHITE_ALBUM).permitAll()
                    .anyRequest().authenticated()
                .and()
                    .httpBasic()
                .and()
                    .logout()
                    .clearAuthentication(true)
                    .deleteCookies("JSESSIONID")
                .and()
                    .exceptionHandling()
                        .authenticationEntryPoint((httpServletRequest, httpServletResponse, e) ->
                                httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED)
                        )
                        .accessDeniedHandler((httpServletRequest, httpServletResponse, e) ->
                                httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN)
                        )
                .and()
                .build();
    }

    /**
     * Метод для конфигурации бина AuthenticationManager.
     * Подстановка пользовательского сервиса с логикой регистрации пользователя
     */
    @Bean
    public AuthenticationManager authManager(HttpSecurity http) throws Exception {
        return http.getSharedObject(AuthenticationManagerBuilder.class)
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder)
                .and().build();
    }
}
