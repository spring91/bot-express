package ru.ershov.botexpress.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;

import static ru.ershov.botexpress.config.OpenAPIConfig.F;

//Swagger http://localhost:8080/api/swagger-ui/index.html#/

@SecurityScheme(
        name = "basic-auth",  //@SecurityRequirement(name = "basic-auth")
        type = SecuritySchemeType.HTTP,
        scheme = "basic"    //basic - имя базовой аутентификации
)
@OpenAPIDefinition(
        info = @Info(title = "Bot API", version = "1.0", description = "Bot Express")
)
@Configuration
public class OpenAPIConfig {


    public static final String F = "l";
}
