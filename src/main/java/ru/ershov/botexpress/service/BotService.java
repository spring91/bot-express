package ru.ershov.botexpress.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.ershov.botexpress.rest.dto.filter.BotFilter;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.rest.dto.response.BotResponseDTO;
import ru.ershov.botexpress.store.entity.Bot;

import java.util.List;

/**
 * Сервис бота
 */
public interface BotService {

    //REST--------------------------------------------------------------------------------------------------------------

    /**
     * Создание записи о боте
     *
     * @param botRqDTO сущность бота с данными для сохранения
     * @return сохраненная DTO бота
     */
    BotResponseDTO create(BotRequestDTO botRqDTO);

    /**
     * Получение записи о боте по идентификатору
     *
     * @param id идентификатор бота
     * @return DTO бота
     */
    BotResponseDTO getById(Long id);

    /**
     * Получение всех записей о ботах по идентификатору парка
     *
     * @param parkId   идентификатор парка
     * @param pageable параметры страницы
     * @return страница с DTO ботов
     */
    Page<BotResponseDTO> getAll(Long parkId, Pageable pageable);

    //SERVICE-----------------------------------------------------------------------------------------------------------

    /**
     * Поиск ботов, подходящих под параметры доставки
     *
     * @param filter модель поиска
     * @return список подходящих ботов
     */
    List<Bot> getAll(BotFilter filter);
}
