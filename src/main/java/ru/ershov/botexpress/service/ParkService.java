package ru.ershov.botexpress.service;

import ru.ershov.botexpress.rest.dto.response.ParkResponseDTO;

/**
 * Сервис парка
 */
public interface ParkService {

    /**
     * Получение записи о парке по идентификатору
     *
     * @param id уникальный идентификатор парка
     * @return DTO парка
     */
    ParkResponseDTO getById(Long id);
}
