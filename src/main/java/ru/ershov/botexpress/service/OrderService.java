package ru.ershov.botexpress.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.ershov.botexpress.rest.dto.filter.OrderFilter;
import ru.ershov.botexpress.rest.dto.request.OrderRequestDTO;
import ru.ershov.botexpress.rest.dto.response.OrderResponseDTO;
import ru.ershov.botexpress.rest.dto.response.OrderSessionResponseDTO;

import java.util.UUID;

/**
 * Сервис заказа
 */
public interface OrderService {

    /**
     * Создание записи о заказе, создание неподтвержденной сессии
     *
     * @param orderRqDTO сущность заказа с данными для сохранения
     * @return сохраненная DTO заказа
     */
    OrderResponseDTO create(OrderRequestDTO orderRqDTO);

    /**
     * Подтверждение заказа, изменение статуса активности сессии
     *
     * @param id идентификатор заказа
     */
    void accept(Long id);

    /**
     * Полная отмена заказа.
     * Выражается с помощью дезактивации сессии заказа
     * и установки времени завершения.
     *
     * @param id идентификатор заказа
     */
    void revoke(Long id);

    /**
     * Получение записи о заказе по идентификатору
     *
     * @param id идентификатор заказа
     * @return DTO заказа
     */
    OrderResponseDTO getById(Long id);

    /**
     * Получение сессий заказа по идентификатору заказа
     *
     * @param orderId  идентификатор заказа
     * @param pageable параметры страницы
     * @return страница с Dto сессий заказа
     */
    Page<OrderSessionResponseDTO> getSessions(Long orderId, Pageable pageable);

    /**
     * Получение заказов по идентификатору пользователя
     *
     * @param userId   идентификатор пользователь
     * @param pageable параметры страницы
     * @return страница с Dto заказов
     */
    Page<OrderResponseDTO> getAll(UUID userId, OrderFilter filter, Pageable pageable);
}
