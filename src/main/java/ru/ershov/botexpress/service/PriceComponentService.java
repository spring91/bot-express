package ru.ershov.botexpress.service;

import ru.ershov.botexpress.store.entity.Order;

import java.math.BigDecimal;

/**
 * Вспомогательный сервис расчета стоимости
 */
public interface PriceComponentService {

    /**
     * Получение стоимости заказа по переданным данным.
     * Общая стоимость рассчитывается в зависимости от след-их параметров:
     * <ol>
     *     <li>Стоимость груза за килограмм</li>
     *     <li>Стоимость расстояния перевозки груза</li>
     * </ol>
     *
     * @param order полностью заполненная сущность заказа
     * @return стоимость
     */
    BigDecimal getCost(Order order);
}
