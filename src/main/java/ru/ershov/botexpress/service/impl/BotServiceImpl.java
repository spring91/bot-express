package ru.ershov.botexpress.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.rest.dto.filter.BotFilter;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.rest.dto.response.BotResponseDTO;
import ru.ershov.botexpress.rest.mapper.BotMapper;
import ru.ershov.botexpress.service.BotService;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.entity.enums.BotStatus;
import ru.ershov.botexpress.store.repository.BotRepository;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;

import static ru.ershov.botexpress.store.repository.specification.BotSpecification.filterBy;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class BotServiceImpl implements BotService {

    private static final String BOT_IS_NOT_FOUND_MSG = "Бот %d не найден";
    private static final String PARK_IS_NOT_FOUND_MSG = "Парк %d не найден";
    private static final String MODEL_IS_NOT_FOUND_MSG = "Модель бота %d не найдена";

    private final BotMapper botMapper;
    private final BotRepository botRepository;

    @Override
    public List<Bot> getAll(@NotNull BotFilter filter) {
        return botRepository.findAll(filterBy(filter));
    }

    @Override
    @Transactional
    public BotResponseDTO create(BotRequestDTO botRqDTO) {
        var bot = botMapper.toEntity(botRqDTO);

        if (bot.getPark() == null)
            throw new EntityNotFoundException(String.format(PARK_IS_NOT_FOUND_MSG, botRqDTO.getParkId()));
        if (bot.getModel() == null)
            throw new EntityNotFoundException(String.format(MODEL_IS_NOT_FOUND_MSG, botRqDTO.getModelId()));

        bot.setStatus(BotStatus.AVAILABLE);
        bot.setMaintenanceCost(BigDecimal.ZERO);
        return botMapper.toDTO(
                botRepository.save(bot)
        );
    }

    @Override
    public BotResponseDTO getById(Long id) {
        return botRepository.findById(id)
                .map(botMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(BOT_IS_NOT_FOUND_MSG, id)));
    }

    @Override
    public Page<BotResponseDTO> getAll(Long parkId, Pageable pageable) {
        return botRepository.findAllByParkId(parkId, pageable)
                .map(botMapper::toDTO);
    }
}
