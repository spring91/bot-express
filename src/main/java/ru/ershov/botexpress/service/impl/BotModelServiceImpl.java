package ru.ershov.botexpress.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.rest.dto.response.BotModelResponseDTO;
import ru.ershov.botexpress.rest.mapper.BotModelMapper;
import ru.ershov.botexpress.service.BotModelService;
import ru.ershov.botexpress.store.repository.BotModelRepository;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class BotModelServiceImpl implements BotModelService {

    private static final String MODEL_IS_NOT_FOUND_MSG = "Модель бота %d не найдена";

    private final BotModelMapper modelMapper;
    private final BotModelRepository modelRepository;

    @Override
    public BotModelResponseDTO getById(Long id) {
        return modelRepository.findById(id)
                .map(modelMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(MODEL_IS_NOT_FOUND_MSG, id)));
    }
}
