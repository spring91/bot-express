package ru.ershov.botexpress.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.service.PriceComponentService;
import ru.ershov.botexpress.store.entity.Order;
import ru.ershov.botexpress.store.entity.PriceComponent;
import ru.ershov.botexpress.store.repository.PriceComponentRepository;

import java.math.BigDecimal;

import static ru.ershov.botexpress.store.entity.enums.PriceComponentName.KG;
import static ru.ershov.botexpress.store.entity.enums.PriceComponentName.KM;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PriceComponentServiceImpl implements PriceComponentService {

    private static final String COST_IS_NOT_FOUND_MSG = "Стоимость по атрибуту %s не найдена";

    private final PriceComponentRepository priceComponentRepository;

    @Override
    public BigDecimal getCost(@NotNull Order order) {

        //стоимость дистанции(стоимость преодоления дистанции до взятия груза не уч-ся)
        var totalDistance = order.getFrom().distance(order.getTo());
        var costTotalDistance = priceComponentRepository.getByName(KM)
                .getPrice()
                .multiply(BigDecimal.valueOf(totalDistance));

        //стоимость веса груза
        var costCapacity = priceComponentRepository.getByName(KG)
                .getPrice()
                .multiply(BigDecimal.valueOf(order.getLoadWeight()));

        return costTotalDistance.add(costCapacity);
    }

    /**
     * Метод по расчету стоимости атрибута из таблицы
     *
     * @param name имя атрибута
     * @return возврат стоимости атрибута по имени
     */
    /*@Cacheable(cacheNames = "price", key = "#name")
    public BigDecimal getCostByName(String name) {
        return priceComponentRepository.getByName(name)
                .map(PriceComponent::getPrice)
                .orElseGet(() -> {
                            log.info(String.format(COST_IS_NOT_FOUND_MSG, name));
                            return BigDecimal.valueOf(0);
                        }
                );
    }*/
}
