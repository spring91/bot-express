package ru.ershov.botexpress.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.rest.dto.filter.BotFilter;
import ru.ershov.botexpress.rest.dto.filter.OrderFilter;
import ru.ershov.botexpress.rest.dto.request.OrderRequestDTO;
import ru.ershov.botexpress.rest.dto.response.OrderResponseDTO;
import ru.ershov.botexpress.rest.dto.response.OrderSessionResponseDTO;
import ru.ershov.botexpress.rest.handler.exception.SessionActivityException;
import ru.ershov.botexpress.rest.mapper.OrderMapper;
import ru.ershov.botexpress.rest.mapper.OrderSessionMapper;
import ru.ershov.botexpress.service.BotService;
import ru.ershov.botexpress.service.OrderService;
import ru.ershov.botexpress.service.PriceComponentService;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.entity.Order;
import ru.ershov.botexpress.store.entity.OrderSession;
import ru.ershov.botexpress.store.repository.OrderRepository;
import ru.ershov.botexpress.store.repository.OrderSessionRepository;
import ru.ershov.botexpress.store.repository.specification.OrderSpecification;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingDouble;
import static java.util.Comparator.comparingLong;
import static ru.ershov.botexpress.store.entity.enums.BotStatus.AVAILABLE;
import static ru.ershov.botexpress.store.entity.enums.BotStatus.IN_OPERATION;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

    private static final String ORDER_IS_NOT_FOUND_MSG = "Заказ %d не найден";
    private static final String ORDER_IS_PROCESSED_MSG = "Заказ %d обрабатывается";
    private static final String ORDER_CANNOT_BE_REVOKED_MSG = "Заказ %d уже отменен";
    private static final String USER_IS_NOT_FOUND_MSG = "Заказчик %s не идентифицирован";
    private static final String NO_SUITABLE_BOT_FOUND_MSG = "Не найден бот под заказ %d";
    private static final String ORDER_IS_NOT_ACCEPTED_MSG = "Заказ %d еще не подтвержден";
    private static final String ORDER_CANNOT_BE_ACCEPTED_MSG = "Заказ %d уже подтвержден";

    private final OrderRepository orderRepository;
    private final OrderSessionRepository sessionRepository;
    private final OrderMapper orderMapper;
    private final OrderSessionMapper sessionMapper;
    private final BotService botService;
    private final PriceComponentService priceComponentService;

    @Override
    @Transactional
    public OrderResponseDTO create(OrderRequestDTO orderRqDTO) {
        var order = orderMapper.toEntity(orderRqDTO);
        order.setProfit(priceComponentService.getCost(order));
        if (order.getUser() == null)
            throw new EntityNotFoundException(USER_IS_NOT_FOUND_MSG);
        return orderMapper.toDTO(orderRepository.save(order));
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void accept(Long id) {
        var order = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ORDER_IS_NOT_FOUND_MSG, id)));
        log.info(String.format(ORDER_IS_PROCESSED_MSG, order.getId()));
        if (order.getSessions().isEmpty()) {
            //поиск бота
            var bot = getAvailableBot(order)
                    .orElseThrow(() ->
                            new EntityNotFoundException(String.format(NO_SUITABLE_BOT_FOUND_MSG, order.getId()))
                    );
            //создание сессии
            createSession(bot, order);
            //запуск бота
            bot.setStatus(IN_OPERATION);
        } else throw new SessionActivityException(String.format(ORDER_CANNOT_BE_ACCEPTED_MSG, id));
    }

    /**
     * Метод поиска ближайшего бота, подходящего под параметры заказа
     *
     * @param order заказ
     * @return возможный бот
     */
    private Optional<Bot> getAvailableBot(@NotNull Order order) {
        return botService.getAll(BotFilter.builder()
                        .canTakeFragile(order.getIsFragile())
                        .loadCapacity(order.getLoadWeight())
                        .trunkVolume(order.getVolume())
                        .build()).stream()
                .filter(bot -> bot.getStatus() == AVAILABLE)
                .min(comparingDouble(o -> o.getPark().getLocation().distance(order.getFrom())));
    }

    /**
     * Метод создания базовой сессии
     *
     * @param bot   бот
     * @param order заказ
     * @return сохраненная сессия
     */
    private OrderSession createSession(@NotNull Bot bot, @NotNull Order order) {
        return sessionRepository.save(
                OrderSession.builder()
                        .start(Instant.now())
                        .isActive(true)
                        .isSuccessful(false)
                        .bot(bot)
                        .order(order)
                        .build()
        );
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void revoke(Long id) {
        var order = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ORDER_IS_NOT_FOUND_MSG, id)));

        var sessions = order.getSessions();
        if (!sessions.isEmpty()) {
            //берем активную сессию(она всегда последняя)
            var lastSession = sessions.stream()
                    .sorted(comparingLong(OrderSession::getId).reversed())
                    .collect(Collectors.toList()).get(0);
            //выключаем и завершаем сессию
            if (lastSession.getFinish() == null) {
                lastSession.setIsActive(false);
                lastSession.setFinish(Instant.now());
                //не забываем и про бота
                lastSession.getBot().setStatus(AVAILABLE);
            } else throw new SessionActivityException(String.format(ORDER_CANNOT_BE_REVOKED_MSG, id));
        } else throw new SessionActivityException(String.format(ORDER_IS_NOT_ACCEPTED_MSG, id));
    }

    @Override
    public OrderResponseDTO getById(Long id) {
        return orderRepository.findById(id)
                .map(orderMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ORDER_IS_NOT_FOUND_MSG, id)));
    }

    @Override
    public Page<OrderSessionResponseDTO> getSessions(Long orderId, Pageable pageable) {
        return sessionRepository.findAllByOrderId(orderId, pageable)
                .map(sessionMapper::toDTO);
    }

    @Override
    public Page<OrderResponseDTO> getAll(UUID userId, OrderFilter filter, Pageable pageable) {
        return orderRepository.findAllByUserId(userId, OrderSpecification.filterBy(filter), pageable)
                .map(orderMapper::toDTO);
    }
}
