package ru.ershov.botexpress.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ershov.botexpress.rest.dto.response.ParkResponseDTO;
import ru.ershov.botexpress.rest.mapper.ParkMapper;
import ru.ershov.botexpress.service.ParkService;
import ru.ershov.botexpress.store.repository.ParkRepository;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ParkServiceImpl implements ParkService {

    private static final String PARK_IS_NOT_FOUND_MSG = "Парк %d не найден";

    private final ParkMapper parkMapper;
    private final ParkRepository parkRepository;

    @Override
    public ParkResponseDTO getById(Long id) {
        return parkRepository.findById(id)
                .map(parkMapper::toDTO)
                .orElseThrow(() -> new EntityNotFoundException(String.format(PARK_IS_NOT_FOUND_MSG, id)));
    }
}
