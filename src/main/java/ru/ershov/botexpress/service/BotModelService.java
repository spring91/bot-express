package ru.ershov.botexpress.service;

import ru.ershov.botexpress.rest.dto.response.BotModelResponseDTO;

/**
 * Сервис модели бота
 */
public interface BotModelService {

    /**
     * Получение записи о модели бота по идентификатору
     *
     * @param id идентификатор модели бота
     * @return DTO модели бота
     */
    BotModelResponseDTO getById(Long id);
}
