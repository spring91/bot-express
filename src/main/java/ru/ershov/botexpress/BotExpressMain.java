package ru.ershov.botexpress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ershov Daniil
 * @version 1.0
 */
@SpringBootApplication
public class BotExpressMain {
    public static void main(String[] args) {
        SpringApplication.run(BotExpressMain.class);
    }
}
