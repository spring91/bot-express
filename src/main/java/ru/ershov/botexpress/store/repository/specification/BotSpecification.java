package ru.ershov.botexpress.store.repository.specification;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.domain.Specification;
import ru.ershov.botexpress.rest.dto.filter.BotFilter;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.entity.BotModel;
import ru.ershov.botexpress.store.entity.BotModel_;
import ru.ershov.botexpress.store.entity.Bot_;

import javax.persistence.criteria.Join;

/**
 * Спецификация сущности бота, использующая сложные Sql запросы через Criteria Api,
 * для фильтрации ботов в зависимости от параметров
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BotSpecification {

    //TODO: не протестировано
    public static Specification<Bot> filterBy(@NotNull BotFilter filter) {
        return Specification.where(isCanTakeFragile(filter.getCanTakeFragile()))
                .and(withLoadCapacity(filter.getLoadCapacity()))
                .and(withTrunkVolume(filter.getTrunkVolume()));
    }

    /**
     * Метод фильтрация по полю возможности перевозки хрупких заказов
     */
    private static Specification<Bot> isCanTakeFragile(Boolean fragile) {
        return (root, query, criteriaBuilder) -> {
            if (fragile != null) {
                Join<Bot, BotModel> joinedBot = root.join(Bot_.MODEL);
                query.multiselect(joinedBot)
                        .where(
                                criteriaBuilder.equal(joinedBot.get(BotModel_.CAN_TAKE_FRAGILE), fragile)
                        );
                return query.getRestriction();
            }
            return criteriaBuilder.conjunction();
        };
    }

    /**
     * Метод фильтрация по полю объема багажника
     */
    private static Specification<Bot> withLoadCapacity(Float loadCapacity) {
        return (root, query, criteriaBuilder) -> {
            if (loadCapacity != null) {
                Join<Bot, BotModel> joinedBot = root.join(Bot_.MODEL);
                query.multiselect(joinedBot)
                        .where(
                                criteriaBuilder.greaterThanOrEqualTo(joinedBot.get(BotModel_.LOAD_CAPACITY), loadCapacity)
                        );
                return query.getRestriction();
            }
            return criteriaBuilder.conjunction();
        };
    }

    /**
     * Метод фильтрация по полю грузоподъемности
     */
    private static Specification<Bot> withTrunkVolume(Float trunkVolume) {
        return (root, query, criteriaBuilder) -> {
            if (trunkVolume != null) {
                Join<Bot, BotModel> joinedBot = root.join(Bot_.MODEL);
                query.multiselect(joinedBot)
                        .where(
                                criteriaBuilder.greaterThanOrEqualTo(joinedBot.get(BotModel_.TRUNK_VOLUME), trunkVolume)
                        );
                return query.getRestriction();
            }
            return criteriaBuilder.conjunction();
        };
    }
}
