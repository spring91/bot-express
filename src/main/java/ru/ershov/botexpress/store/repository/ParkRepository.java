package ru.ershov.botexpress.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ershov.botexpress.store.entity.Park;

@Repository
public interface ParkRepository extends JpaRepository<Park, Long>{
}
