package ru.ershov.botexpress.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ershov.botexpress.store.entity.BotModel;

@Repository
public interface BotModelRepository extends JpaRepository<BotModel, Long> {
}
