package ru.ershov.botexpress.store.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.ershov.botexpress.store.entity.Bot;

@Repository
public interface BotRepository extends JpaRepository<Bot, Long>, JpaSpecificationExecutor<Bot> {

    Page<Bot> findAllByParkId(Long id, Pageable pageable);
}
