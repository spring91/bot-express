package ru.ershov.botexpress.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ershov.botexpress.store.entity.PriceComponent;
import ru.ershov.botexpress.store.entity.enums.PriceComponentName;

@Repository
public interface PriceComponentRepository extends JpaRepository<PriceComponent, Long> {
    PriceComponent getByName(PriceComponentName name);
}
