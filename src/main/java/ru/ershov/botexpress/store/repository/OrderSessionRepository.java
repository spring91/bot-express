package ru.ershov.botexpress.store.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ershov.botexpress.store.entity.OrderSession;

@Repository
public interface OrderSessionRepository extends JpaRepository<OrderSession, Long> {

    Page<OrderSession> findAllByOrderId(Long id, Pageable pageable);
}
