package ru.ershov.botexpress.store.repository.specification;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.domain.Specification;
import ru.ershov.botexpress.rest.dto.filter.OrderFilter;
import ru.ershov.botexpress.store.entity.*;

import javax.persistence.criteria.Join;
import java.math.BigDecimal;

/**
 * Спецификация сущности заказа, использующая сложные Sql запросы через Criteria Api,
 * для фильтрации заказов в зависимости от параметров
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderSpecification {

    public static Specification<Order> filterBy(@NotNull OrderFilter filter) {
        return Specification.where(isFragile(filter.getIsFragile()))
                .and(withLoadWeight(filter.getLoadWeight()))
                .and(withVolume(filter.getVolume()))
                .and(withProfit(filter.getProfit()));
    }

    /**
     * Метод фильтрация по полю хрупкости заказа
     */
    private static Specification<Order> isFragile(Boolean isFragile) {
        return isFragile == null ? null : (root, query, builder) -> {
            if (isFragile) return builder.equal(root.get(Order_.IS_FRAGILE), true);
            return builder.notEqual(root.get(Order_.IS_FRAGILE), false);
        };
    }

    /**
     * Метод фильтрация по полю объема заказа
     */
    private static Specification<Order> withLoadWeight(Float loadWeight) {
        return loadWeight == null ? null : (root, query, criteriaBuilder)
                -> criteriaBuilder.greaterThanOrEqualTo(root.get(Order_.LOAD_WEIGHT), loadWeight);
    }

    /**
     * Метод фильтрация по полю массы груза
     */
    private static Specification<Order> withVolume(Float volume) {
        return volume == null ? null : (root, query, criteriaBuilder)
                -> criteriaBuilder.greaterThanOrEqualTo(root.get(Order_.VOLUME), volume);
    }

    /**
     * Метод фильтрация по полю стоимости заказа
     */
    private static Specification<Order> withProfit(BigDecimal profit) {
        return profit == null ? null : (root, query, criteriaBuilder)
                -> criteriaBuilder.greaterThanOrEqualTo(root.get(Order_.PROFIT), profit);
    }
}
