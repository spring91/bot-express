package ru.ershov.botexpress.store.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Сущность модели робота
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BotModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Наименование модели бота
     */
    private String name;

    /**
     * Скорость модели бота
     */
    private Integer speed;

    /**
     * Защита корпуса для перевоза хрупких грузов
     */
    private Boolean canTakeFragile;

    /**
     * Грузоподъемность модели бота
     */
    private Float loadCapacity;

    /**
     * Объем багажника
     */
    private Float trunkVolume;

    /**
     * Боты данной модели
     */
    @OneToMany(mappedBy = "model", cascade = CascadeType.REMOVE)
    private List<Bot> bots;
}
