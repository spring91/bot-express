package ru.ershov.botexpress.store.entity;

import lombok.*;
import ru.ershov.botexpress.store.entity.enums.BotStatus;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Сущность робота
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Статус бота
     */
    @Enumerated(EnumType.STRING)
    private BotStatus status;

    /**
     * Затраты на обслуживание бота в данный момент
     */
    private BigDecimal maintenanceCost;

    /**
     * Модель бота
     */
    @ManyToOne
    @JoinColumn(name = "model_id")
    private BotModel model;

    /**
     * Парк, которому принадлежит бот
     */
    @ManyToOne
    @JoinColumn(name = "park_id")
    private Park park;
}
