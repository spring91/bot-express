package ru.ershov.botexpress.store.entity;

import lombok.*;
import ru.ershov.botexpress.security.store.entity.UserEntity;
import ru.ershov.botexpress.store.entity.location.Location;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Сущность заказа
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"order\"")
public class Order {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * Координаты локации, из которой доставляется груз
     */
    @Embedded
    @AttributeOverride(name = "latitude", column = @Column(name = "from_latitude"))
    @AttributeOverride(name = "longitude", column = @Column(name = "from_longitude"))
    private Location from;

    /**
     * Координаты локации, в которую доставляется груз
     */
    @Embedded
    @AttributeOverride(name = "latitude", column = @Column(name = "to_latitude"))
    @AttributeOverride(name = "longitude", column = @Column(name = "to_longitude"))
    private Location to;

    /**
     * Груз хрупкий / нехрупкий
     */
    private Boolean isFragile;

    /**
     * Вес груза
     */
    private Float loadWeight;

    /**
     * Объем груза
     */
    private Float volume;

    /**
     * Доход от доставки заказа.
     */
    private BigDecimal profit;

    /**
     * Пользователь, сделавший заказ
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    /**
     * Сессии заказа.
     * Сессий у заказа может быть несколько, например, если бот сломается,
     * то при подмене бота сессия создастся заново.
     */
    @OneToMany(mappedBy = "order", cascade = CascadeType.REMOVE)
    private List<OrderSession> sessions;
}
