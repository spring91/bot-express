package ru.ershov.botexpress.store.entity.enums;

/**
 * Имя ценового компонента
 */
public enum PriceComponentName {

    KM, KG
}
