package ru.ershov.botexpress.store.entity;

import lombok.*;
import ru.ershov.botexpress.store.entity.enums.PriceComponentName;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Сущность стоимости
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Наименование атрибута
     */
    @Enumerated(EnumType.STRING)
    private PriceComponentName name;

    /**
     * Цена атрибута
     */
    private BigDecimal price;
}
