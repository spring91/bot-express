package ru.ershov.botexpress.store.entity;

import lombok.*;
import ru.ershov.botexpress.store.entity.location.Location;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Сущность парка
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Park {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Наименование парка
     */
    private String name;

    /**
     * Местоположение парка с ботами (координаты)
     */
    @Embedded
    private Location location;

    /**
     * Прибыль парка
     */
    private BigDecimal profits;

    /**
     * Расходы на ТО ботов данного парка
     */
    private BigDecimal totalMaintenanceCosts;

    /**
     * Боты в парке
     */
    @OneToMany(mappedBy = "park")
    private List<Bot> bots;
}
