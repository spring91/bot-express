package ru.ershov.botexpress.store.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

/**
 * Сущность сессии заказа
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"session\"")
public class OrderSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Время открытия сессии
     */
    private Instant start;

    /**
     * Время закрытия сессии
     */
    private Instant finish;

    /**
     * Сессия активна / закрыта
     */
    private Boolean isActive;

    /**
     * Сессия успешна / не успешна
     */
    private Boolean isSuccessful;

    /**
     * Бот, доставляющий заказ
     */
    @ManyToOne
    @JoinColumn(name = "bot_id")
    private Bot bot;

    /**
     * Заказ, который доставляется ботом
     */
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
}
