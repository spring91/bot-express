package ru.ershov.botexpress.store.entity.location;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Embeddable;

/**
 * Координаты локации
 */
@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    /**
     * Широта
     */
    private Double latitude;

    /**
     * Долгота
     */
    private Double longitude;

    /**
     * Формула расчета длины дуги - "модифицированная формула гавер-синусов".
     *
     * @return дистанция между двумя точками
     */
    public double distance(@NotNull Location location) {

        //координаты двух точек
        double x1 = this.latitude;
        double y1 = this.longitude;

        double x2 = location.latitude;
        double y2 = location.longitude;

        //в радианах
        double radX1 = x1 * Math.PI / 180;
        double radY1 = y1 * Math.PI / 180;
        double radX2 = x2 * Math.PI / 180;
        double radY2 = y2 * Math.PI / 180;

        //косинусы и синусы широт и разницы долготы
        double cosX1 = Math.cos(radX1);
        double cosX2 = Math.cos(radX2);
        double sinX1 = Math.sin(radX1);
        double sinX2 = Math.sin(radX2);
        double delta = radY2 - radY1;
        double cosDelta = Math.cos(delta);
        double sinDelta = Math.sin(delta);

        //вычисления длины большого круга
        double y = Math.sqrt(Math.pow(cosX2 * sinDelta, 2)
                + Math.pow(cosX1 * sinX2 - sinX1 * cosX2 * cosDelta, 2));
        double x = sinX1 * sinX2 + cosX1 * cosX2 * cosDelta;
        double ad = Math.atan2(y, x);

        //радианы
        return ad * 6372795;
    }
}
