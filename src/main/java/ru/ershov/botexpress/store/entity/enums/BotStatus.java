package ru.ershov.botexpress.store.entity.enums;

/**
 * Статус бота
 */
public enum BotStatus {

    AVAILABLE,
    IN_OPERATION,
    MAINTENANCE
}
