package ru.ershov.botexpress.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import ru.ershov.botexpress.rest.api.BotModelAPI;
import ru.ershov.botexpress.rest.dto.response.BotModelResponseDTO;
import ru.ershov.botexpress.service.BotModelService;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BotModelController implements BotModelAPI {

    private final BotModelService botModelService;

    @Override
    public BotModelResponseDTO getById(Long id) {
        log.info("GET:/bot_models/{id} : id={}", id);
        return botModelService.getById(id);
    }
}
