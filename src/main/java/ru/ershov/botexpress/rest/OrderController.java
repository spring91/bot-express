package ru.ershov.botexpress.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.ershov.botexpress.rest.api.OrderAPI;
import ru.ershov.botexpress.rest.dto.filter.OrderFilter;
import ru.ershov.botexpress.rest.dto.request.OrderRequestDTO;
import ru.ershov.botexpress.rest.dto.response.OrderResponseDTO;
import ru.ershov.botexpress.rest.dto.response.OrderSessionResponseDTO;
import ru.ershov.botexpress.security.store.entity.securityprovider.SecurityUser;
import ru.ershov.botexpress.service.OrderService;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderController implements OrderAPI {

    private final OrderService orderService;

    @Override
    public ResponseEntity<OrderResponseDTO> create(OrderRequestDTO orderRqDto, SecurityUser user) {
        log.info("POST:/orders : orderRqDto={}", orderRqDto);

        orderRqDto.setUserId(user.getId());
        var orderRsDto = orderService.create(orderRqDto);
        var url = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(orderRsDto.getId())
                .toUri();
        return ResponseEntity.created(url).body(orderRsDto);
    }

    @Override
    public void accept(Long id) {
        log.info("PATCH:/orders/{id}/accept : id={}", id);
        orderService.accept(id);
    }

    @Override
    public void revoke(Long id) {
        log.info("PATCH:/orders/{id}/revoke : id={}", id);
        orderService.revoke(id);
    }

    @Override
    public OrderResponseDTO getById(Long id) {
        log.info("GET:/orders/{id} : id={}", id);
        return orderService.getById(id);
    }

    @Override
    public Page<OrderSessionResponseDTO> getSessions(Long orderId, Pageable pageable) {
        log.info("GET:/orders/{id}/sessions : id={}, pageable={}", orderId, pageable);
        return orderService.getSessions(orderId, pageable);
    }

    @Override
    public Page<OrderResponseDTO> getAll(UUID userId, OrderFilter filter, Pageable pageable) {
        log.info("GET:/orders/ : userId={}, filter={}, pageable={}", userId, filter, pageable);
        return orderService.getAll(userId, filter, pageable);
    }
}
