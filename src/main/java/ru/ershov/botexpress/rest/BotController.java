package ru.ershov.botexpress.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.ershov.botexpress.rest.api.BotAPI;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.rest.dto.response.BotResponseDTO;
import ru.ershov.botexpress.service.BotService;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BotController implements BotAPI {

    private final BotService botService;

    @Override
    public ResponseEntity<BotResponseDTO> create(BotRequestDTO botRqDto) {
        log.info("POST:/bots : botRqDto={}", botRqDto);

        var botRsDto = botService.create(botRqDto);
        var url = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(botRsDto.getId())
                .toUri();
        return ResponseEntity.created(url).body(botRsDto);
    }

    @Override
    public BotResponseDTO getById(Long id) {
        log.info("GET:/bots/{id} : id={}", id);
        return botService.getById(id);
    }

    @Override
    public Page<BotResponseDTO> getAll(Long parkId, Pageable pageable) {
        log.info("GET:/bots/from-park/{parkId} : parkId={}, pageable={}", parkId, pageable);
        return botService.getAll(parkId, pageable);
    }
}
