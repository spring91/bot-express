package ru.ershov.botexpress.rest.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ershov.botexpress.rest.dto.response.ParkResponseDTO;

import javax.validation.constraints.Positive;

@Validated
@RequestMapping("/parks")
@SecurityRequirement(name = "basic-auth")
@Tag(name = "Парки", description = "Контроллер для управления парками")
@PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
public interface ParkAPI {

    @Operation(summary = "Метод получения парка по идентификатору", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ParkResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping("/{id}")
    ParkResponseDTO getById(@PathVariable @Positive Long id);
}
