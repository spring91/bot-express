package ru.ershov.botexpress.rest.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.ershov.botexpress.rest.dto.filter.OrderFilter;
import ru.ershov.botexpress.rest.dto.request.OrderRequestDTO;
import ru.ershov.botexpress.rest.dto.response.OrderResponseDTO;
import ru.ershov.botexpress.rest.dto.response.OrderSessionResponseDTO;
import ru.ershov.botexpress.security.store.entity.securityprovider.SecurityUser;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Validated
@RequestMapping("/orders")
@SecurityRequirement(name = "basic-auth")
@Tag(name = "Заказы", description = "Контроллер для управления заказами")
@PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
public interface OrderAPI {
    @Operation(summary = "Метод создания заказа", method = "POST",
            responses = {
                    @ApiResponse(description = "CREATED", responseCode = "201",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = OrderResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403")
            }
    )
    @PostMapping
    @PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_USER)")
    ResponseEntity<OrderResponseDTO> create(@RequestBody @Valid OrderRequestDTO orderRqDto,
                                            @Parameter(hidden = true) @AuthenticationPrincipal SecurityUser user);

    @Operation(summary = "Метод подтверждения заказа по идентификатору", method = "PATCH",

            responses = {
                    @ApiResponse(description = "ACCEPTED", responseCode = "202"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @PatchMapping("/{id}/accept")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("@orderServiceImpl.getById(#id).getUserId() == authentication.getPrincipal().getId()")
    void accept(@PathVariable @Positive Long id);

    @Operation(summary = "Метод отмены заказа по идентификатору", method = "PATCH",
            responses = {
                    @ApiResponse(description = "ACCEPTED", responseCode = "202"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @PatchMapping("/{id}/revoke")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("@orderServiceImpl.getById(#id).getUserId() == authentication.getPrincipal().getId()")
    void revoke(@PathVariable @Positive Long id);

    @Operation(summary = "Метод получения заказа по идентификатору", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = OrderResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping("/{id}")
    @PostAuthorize("returnObject.getUserId() == authentication.getPrincipal().getId() " +
            "or hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
    OrderResponseDTO getById(@PathVariable @Positive Long id);

    @Operation(summary = "Метод получения сессий по идентификатору заказа", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping("/{id}/sessions")
    @PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
    Page<OrderSessionResponseDTO> getSessions(@PathVariable(name = "id") @Positive Long orderId,
                                              @ParameterObject Pageable pageable);

    @Operation(summary = "Получение заказов пользователя", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping
    @PreAuthorize("#userId == authentication.getPrincipal().getId() " +
            "or hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
    Page<OrderResponseDTO> getAll(@RequestParam @NotNull UUID userId,
                                  @ModelAttribute OrderFilter filter,
                                  @ParameterObject Pageable pageable);
}
