package ru.ershov.botexpress.rest.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.rest.dto.response.BotResponseDTO;

import javax.validation.constraints.Positive;

@Validated
@RequestMapping("/bots")
@SecurityRequirement(name = "basic-auth")
@Tag(name = "Боты", description = "Контроллер для управления ботами")
@PreAuthorize("hasRole(T(ru.ershov.botexpress.security.store.entity.enums.RoleName).ROLE_ADMIN)")
public interface BotAPI {

    @Operation(summary = "Метод создания бота", method = "POST",
            responses = {
                    @ApiResponse(description = "CREATED", responseCode = "201",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = BotResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403")
            }
    )
    @PostMapping
    ResponseEntity<BotResponseDTO> create(@RequestBody BotRequestDTO botRqDto);

    @Operation(summary = "Метод получения бота по идентификатору", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = BotResponseDTO.class))),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping("/{id}")
    BotResponseDTO getById(@PathVariable @Positive Long id);

    @Operation(summary = "Получение всех ботов по идентификатору парка", method = "GET",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "BAD REQUEST", responseCode = "400"),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401"),
                    @ApiResponse(description = "FORBIDDEN", responseCode = "403"),
                    @ApiResponse(description = "NOT FOUND", responseCode = "404")
            }
    )
    @GetMapping
    Page<BotResponseDTO> getAll(@RequestParam @Positive Long parkId,
                                @ParameterObject Pageable pageable);
}
