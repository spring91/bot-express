package ru.ershov.botexpress.rest.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.ershov.botexpress.rest.handler.exception.SessionActivityException;

import javax.persistence.EntityNotFoundException;

/**
 * Контроллер, который ловит ошибки и отправляет пользователю информацию о них
 */
@RestControllerAdvice
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    /**
     * Метод обрабатывает пользовательское исключение SessionActivityException
     *
     * @param e исключение SessionActivityException
     * @return http-ответ с кодом 400 и телом из содержимого SessionActivityException
     */
    @ExceptionHandler(SessionActivityException.class)
    public ResponseEntity<String> sessionActivityException(SessionActivityException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.getMessage());
    }

    /**
     * Метод обрабатывает исключение EntityNotFoundException
     *
     * @param e исключение EntityNotFoundException
     * @return http-ответ с кодом 404 и телом из содержимого EntityNotFoundException
     */
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<String> entityNotFoundException(EntityNotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
    }

    /**
     * Метод обрабатывает базовое исключение MethodArgumentNotValidException
     *
     * @param ex исключение MethodArgumentNotValidException
     * @return http-ответ с кодом 400 и телом из содержимого MethodArgumentNotValidException
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        return ResponseEntity
                .status(status)
                .body("Method Argument Not Valid : \n" + ex.getLocalizedMessage());
    }
}