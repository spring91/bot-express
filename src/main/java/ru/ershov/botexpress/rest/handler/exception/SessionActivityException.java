package ru.ershov.botexpress.rest.handler.exception;

/**
 * Ошибка сессии заказа
 */
public class SessionActivityException extends RuntimeException {
    public SessionActivityException(String message) {
        super(message);
    }
}
