package ru.ershov.botexpress.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ershov.botexpress.store.entity.enums.BotStatus;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответное DTO бота")
public class BotResponseDTO {

    private Long id;

    private BotStatus status;

    private BigDecimal maintenanceCost;

    private Long modelId;

    private Long parkId;
}
