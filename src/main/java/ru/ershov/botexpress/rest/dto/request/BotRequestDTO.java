package ru.ershov.botexpress.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Входное DTO бота")
public class BotRequestDTO {

    @NotNull(message = "Не указана модель бота")
    @Schema(description = "Модель к которому относится бот")
    private Long modelId;

    @NotNull(message = "Не указана модель парка")
    @Schema(description = "Парк к которому относится бот")
    private Long parkId;
}
