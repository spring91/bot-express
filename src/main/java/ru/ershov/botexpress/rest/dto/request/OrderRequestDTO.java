package ru.ershov.botexpress.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Входное DTO заказа")
public class OrderRequestDTO {

    @Min(value = 0)
    @Max(value = 90)
    @NotNull(message = "Не указан адрес, откуда забрать заказа")
    @Schema(description = "Широта точки подбора заказа")
    private Double latitudeFrom;

    @Min(value = 0)
    @Max(value = 180)
    @NotNull(message = "Не указан адрес, откуда забрать заказа")
    @Schema(description = "Долгота точки подбора заказа")
    private Double longitudeFrom;

    @Min(value = 0)
    @Max(value = 90)
    @NotNull(message = "Не указан адрес, куда доставить заказ")
    @Schema(description = "Широта точки доставки заказа")
    private Double latitudeTo;

    @Min(value = 0)
    @Max(value = 180)
    @NotNull(message = "Не указан адрес, куда доставить заказ")
    @Schema(description = "Долгота точки доставки заказа")
    private Double longitudeTo;

    @NotNull(message = "Не указана возможность перевоза хрупких материалов")
    @Schema(description = "Возможность перевоза хрупких материалов")
    private Boolean isFragile;

    @Positive(message = "Вес груза должен быть положителен")
    @NotNull(message = "Не указан вес посылки")
    @Schema(description = "Вес груза")
    private Float loadWeight;

    @Positive(message = "Объем груза должен быть положителен")
    @NotNull(message = "Не указан объем посылки")
    @Schema(description = "Объем груза")
    private Float volume;

    @Schema(description = "Юзер к которому относится заказ",
            accessMode = Schema.AccessMode.READ_ONLY)
    private UUID userId;
}
