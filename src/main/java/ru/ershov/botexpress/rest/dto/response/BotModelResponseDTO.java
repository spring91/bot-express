package ru.ershov.botexpress.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответное DTO модели бота")
public class BotModelResponseDTO {

    private Long id;

    private String name;

    private Integer speed;

    private Boolean canTakeFragile;

    private Float loadCapacity;

    private Float trunkVolume;
}
