package ru.ershov.botexpress.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ershov.botexpress.store.entity.location.Location;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Выходное DTO заказа")
public class OrderResponseDTO {

    private Long id;

    private Location from;

    private Location to;

    private Boolean isFragile;

    private Float loadWeight;

    private Float volume;

    private BigDecimal profit;

    @Builder.Default
    private Boolean isActive = false;

    @Builder.Default
    private Boolean isSuccessful = false;

    private UUID userId;
}
