package ru.ershov.botexpress.rest.dto.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BotFilter {

    private Boolean canTakeFragile;

    private Float loadCapacity;

    private Float trunkVolume;
}
