package ru.ershov.botexpress.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Выходное DTO сессии заказа")
public class OrderSessionResponseDTO {

    private Long id;

    private Instant start;

    private Instant finish;

    private Boolean isActive;

    private Boolean isSuccessful;

    private Long botId;
}
