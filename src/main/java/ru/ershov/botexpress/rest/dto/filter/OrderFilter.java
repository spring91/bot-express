package ru.ershov.botexpress.rest.dto.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderFilter {

    private Boolean isFragile;

    private Float loadWeight;

    private Float volume;

    private BigDecimal profit;
}
