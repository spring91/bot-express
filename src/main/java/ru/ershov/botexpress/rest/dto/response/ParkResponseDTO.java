package ru.ershov.botexpress.rest.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ershov.botexpress.store.entity.location.Location;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Выходное DTO парка")
public class ParkResponseDTO {

    private Long id;

    private String name;

    private Location location;

    private BigDecimal profits;

    private BigDecimal totalMaintenanceCosts;
}
