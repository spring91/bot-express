package ru.ershov.botexpress.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import ru.ershov.botexpress.rest.api.ParkAPI;
import ru.ershov.botexpress.rest.dto.response.ParkResponseDTO;
import ru.ershov.botexpress.service.ParkService;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ParkController implements ParkAPI {

    private final ParkService parkService;

    @Override
    public ParkResponseDTO getById(Long id) {
        log.info("GET:/park/{id} : id={}", id);
        return parkService.getById(id);
    }
}
