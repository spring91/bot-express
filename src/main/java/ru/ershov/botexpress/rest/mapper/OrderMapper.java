package ru.ershov.botexpress.rest.mapper;

import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ershov.botexpress.rest.dto.request.OrderRequestDTO;
import ru.ershov.botexpress.rest.dto.response.OrderResponseDTO;
import ru.ershov.botexpress.security.store.repository.UserRepository;
import ru.ershov.botexpress.store.entity.Order;
import ru.ershov.botexpress.store.entity.OrderSession;
import ru.ershov.botexpress.store.entity.location.Location;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingLong;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class OrderMapper {

    @Autowired
    protected OrderSessionMapper sessionMapper;
    @Autowired
    protected UserRepository userRepository;

    @Mapping(target = "userId", expression = "java(order.getUser().getId())")
    public abstract OrderResponseDTO toDTO(Order order);

    @AfterMapping
    protected void mapIsActive(Order order, @MappingTarget OrderResponseDTO orderRsDTO) {
        var sessions = getSession(order);
        if (!sessions.isEmpty()) {
            orderRsDTO.setIsActive(sessions.get(0).getIsActive());
        }
    }

    @AfterMapping
    protected void mapIsSuccessful(Order order, @MappingTarget OrderResponseDTO orderRsDTO) {
        var sessions = getSession(order);
        if (!sessions.isEmpty()) {
            orderRsDTO.setIsSuccessful(sessions.get(0).getIsSuccessful());
        }
    }

    /**
     * Метод получения сортированных сессий заказа
     *
     * @param order заказ
     * @return список сессий
     */
    private List<OrderSession> getSession(Order order) {
        return Optional.ofNullable(order.getSessions())
                .map(sessions -> sessions.stream()
                        .sorted(comparingLong(OrderSession::getId).reversed())
                        .collect(Collectors.toList())
                )
                .orElse(Collections.emptyList());
    }

    public abstract Order toEntity(OrderRequestDTO orderRqDTO);

    @AfterMapping
    protected void mapLocationFrom(OrderRequestDTO orderRqDTO, @MappingTarget Order order) {
        order.setFrom(Location.builder()
                .latitude(orderRqDTO.getLatitudeFrom())
                .longitude(orderRqDTO.getLongitudeFrom())
                .build()
        );
    }

    @AfterMapping
    protected void mapLocationTo(OrderRequestDTO orderRqDTO, @MappingTarget Order order) {
        order.setTo(Location.builder()
                .latitude(orderRqDTO.getLatitudeTo())
                .longitude(orderRqDTO.getLongitudeTo())
                .build()
        );
    }

    @AfterMapping
    protected void mapUser(OrderRequestDTO orderRqDTO, @MappingTarget Order order) {
        Optional.ofNullable(orderRqDTO.getUserId())
                .flatMap(userId -> userRepository.findById(userId))
                .ifPresent(order::setUser);
    }
}