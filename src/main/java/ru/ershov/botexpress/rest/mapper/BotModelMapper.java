package ru.ershov.botexpress.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.ershov.botexpress.rest.dto.response.BotModelResponseDTO;
import ru.ershov.botexpress.store.entity.BotModel;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BotModelMapper {

    BotModelResponseDTO toDTO(BotModel model);
}
