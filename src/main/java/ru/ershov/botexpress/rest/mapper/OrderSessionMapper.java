package ru.ershov.botexpress.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.ershov.botexpress.rest.dto.response.OrderSessionResponseDTO;
import ru.ershov.botexpress.store.entity.OrderSession;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderSessionMapper {

    @Mapping(target = "botId", expression = "java(session.getBot().getId())")
    OrderSessionResponseDTO toDTO(OrderSession session);
}
