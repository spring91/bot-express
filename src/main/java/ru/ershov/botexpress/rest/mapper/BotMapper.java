package ru.ershov.botexpress.rest.mapper;

import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ershov.botexpress.rest.dto.request.BotRequestDTO;
import ru.ershov.botexpress.rest.dto.response.BotResponseDTO;
import ru.ershov.botexpress.store.entity.Bot;
import ru.ershov.botexpress.store.repository.BotModelRepository;
import ru.ershov.botexpress.store.repository.ParkRepository;

import java.util.Optional;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class BotMapper {

    @Autowired
    protected BotModelRepository modelRepository;
    @Autowired
    protected ParkRepository parkRepository;

    @Mapping(target = "modelId", expression = "java(bot.getModel().getId())")
    @Mapping(target = "parkId", expression = "java(bot.getPark().getId())")
    public abstract BotResponseDTO toDTO(Bot bot);

    public abstract Bot toEntity(BotRequestDTO botDTO);

    @AfterMapping
    protected void mapModel(BotRequestDTO botDTO, @MappingTarget Bot bot) {
        Optional.ofNullable(botDTO.getModelId())
                .flatMap(modelId -> modelRepository.findById(modelId))
                .ifPresent(bot::setModel);
    }

    @AfterMapping
    protected void mapPark(BotRequestDTO botDTO, @MappingTarget Bot bot) {
        Optional.ofNullable(botDTO.getParkId())
                .flatMap(parkId -> parkRepository.findById(parkId))
                .ifPresent(bot::setPark);
    }
}
