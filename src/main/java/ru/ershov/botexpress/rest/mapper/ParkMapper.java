package ru.ershov.botexpress.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.ershov.botexpress.rest.dto.response.ParkResponseDTO;
import ru.ershov.botexpress.store.entity.Park;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ParkMapper {

    ParkResponseDTO toDTO(Park park);
}
