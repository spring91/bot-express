#Родительский образ
#создает слой с окружением java на alpine linux
FROM adoptopenjdk/openjdk11:alpine
MAINTAINER danny
#прописывает аргументы
ARG JAR_FILE=target/*.jar

#добавление к прослойке linux-системы кэша
RUN apk add --no-cache bash

#перед этим запаковать проект в jar файл
#копирование готового jar'ника в образ докера
COPY ${JAR_FILE} bot-express.jar
#команда запуска джарника
ENTRYPOINT java -jar /bot-express.jar
